from django.views.generic import DetailView
from django.shortcuts import render

from .models import Page
from core.models import SiteSettings


def index(request):
    #settings = SiteSettings.objects.all()[0]
    context = {
        #'settings': settings,
        'current': 'index',
        }

    return render(request, 'index.html', context)

class PageDetailView(DetailView):
    template_name = 'pages/detail.html'
    model = Page
    slug_field = 'slug'
