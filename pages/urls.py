from django.urls import path

from .views import PageDetailView, index


app_name = 'pages'


urlpatterns = [
    path('', index, name='index'),
    path('<slug:slug>/', PageDetailView.as_view(), name='detail'),
]
