# Generated by Django 3.1.5 on 2021-01-26 11:17

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sitesettings',
            name='seo_description',
            field=models.CharField(blank=True, max_length=300, null=True, validators=[django.core.validators.MaxLengthValidator(300)]),
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='seo_keywords',
            field=models.CharField(blank=True, max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='seo_title',
            field=models.CharField(blank=True, max_length=70, null=True, validators=[django.core.validators.MaxLengthValidator(70)]),
        ),
        migrations.AddField(
            model_name='sitesettings',
            name='thumbnail',
            field=models.ImageField(blank=True, null=True, upload_to='thumbnail', verbose_name='Seo Thumbnail'),
        ),
    ]
