from django.db import models
from django.utils.translation import gettext as _

from .abstract_models import SingletonAbstract
from seo.models import SeoModel


class SiteSettings(SingletonAbstract, SeoModel):
    logo = models.ImageField(_('Site Logo'), upload_to="logo", null=True, blank=True)
    name = models.CharField(_('Site Name'), max_length=100)
    desc = models.CharField(_('Site Description'), max_length=400, null=True, blank=True)
    address = models.TextField(_('Address'), null=True, blank=True)
    contact_email = models.EmailField(_('Contact Email'))
    admin_email = models.EmailField(_('Admin Email'), null=True, blank=True)
    phone = models.CharField(_('Phone'), max_length=20, null=True, blank=True)
    footer = models.TextField(_('Footer Text'), null=True, blank=True)
    thumbnail = models.ImageField(
        _("Seo Thumbnail"),
        upload_to="thumbnail",
        blank=True,
        null=True,
    )

    class Meta:
        verbose_name = _('Site Settings')
        verbose_name_plural = _("Site Settings")

    def __str__(self):
        return _('Site Settings')
