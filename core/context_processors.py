from core.models import SiteSettings


def site_settings_context(request):
    return {
        "settings": SiteSettings.objects.first(),
    }
